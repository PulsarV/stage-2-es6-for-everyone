import { controls } from '../../constants/controls';

const healths = new Map();
const blocks = new Map();
const criticalHitCombinations = new Map();

export async function fight(firstFighter, secondFighter) {
  healths.set('first', firstFighter.health);
  healths.set('second', secondFighter.health);

  blocks.set('first', null);
  blocks.set('second', null);

  document.addEventListener('keypress', key => handleKeyPress(key, firstFighter, secondFighter));
  document.addEventListener('keyup', key => handleKeyUp(key, firstFighter, secondFighter));

  return new Promise((resolve) => {
    setInterval(() => {
      if (healths.get('first') <= 0) {
        resolve(secondFighter);
      }

      if (healths.get('second') <= 0) {
        resolve(firstFighter);
      }
    }, 10);
  });
}

export function getDamage(attacker, defender) {
  let damage;

  if (criticalHitCombinations.has(attacker) && criticalHitCombinations.get(attacker).get('keyCodes').size === 3) {
    criticalHitCombinations.get(attacker).set('lastUsed', Date.now());
    criticalHitCombinations.get(attacker).get('keyCodes').clear();

    return 2 * attacker.attack;
  }

  damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = 1 + Math.random();

  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = 1 + Math.random();

  return fighter.defense * dodgeChance;
}

function handleKeyPress(key, firstFighter, secondFighter) {
  switch (true) {
    case key.code === controls.PlayerOneAttack:
      if (!blocks.get('second')) {
        healths.set('second', healths.get('second') - getDamage(firstFighter, secondFighter));
      }
      break;
    case key.code === controls.PlayerOneBlock:
      blocks.set('first', true);
      break;
    case controls.PlayerOneCriticalHitCombination.includes(key.code):
      if (handleCriticalHitCombination(firstFighter, key.code)) {
        healths.set('second', healths.get('second') - getDamage(firstFighter, secondFighter));
      }
      break;
    case key.code === controls.PlayerTwoAttack:
      if (!blocks.get('first')) {
        healths.set('first', healths.get('first') - getDamage(secondFighter, firstFighter));
      }
      break;
    case key.code === controls.PlayerTwoBlock:
      blocks.set('second', true);
      break;
    case controls.PlayerTwoCriticalHitCombination.includes(key.code):
      if (handleCriticalHitCombination(secondFighter, key.code)) {
        healths.set('first', healths.get('first') - getDamage(secondFighter, firstFighter));
      }
      break;
  }

  if (healths.get('first') < 0) {
    healths.get('first').set(0);
  }

  if (healths.get('second') < 0) {
    healths.get('second').set(0);
  }

  updateBars(firstFighter, secondFighter);
}

function handleKeyUp(key, firstFighter, secondFighter) {
  if (key.code === controls.PlayerOneBlock) {
    blocks.set('first', null);
  }

  if (key.code === controls.PlayerTwoBlock) {
    blocks.set('second', null);
  }

  if (controls.PlayerOneCriticalHitCombination.includes(key.code)) {
    criticalHitCombinations.get(firstFighter).get('keyCodes').delete(key.code);
  }

  if (controls.PlayerTwoCriticalHitCombination.includes(key.code)) {
    criticalHitCombinations.get(secondFighter).get('keyCodes').delete(key.code);
  }
}

function handleCriticalHitCombination(fighter, keyCode) {
  const CRITICAL_COMBINATION_USE_TIMEOUT = 1000 * 10;

  if (!criticalHitCombinations.get(fighter)) {
     criticalHitCombinations.set(fighter, new Map());
     criticalHitCombinations.get(fighter).set('keyCodes', new Set([keyCode]));
     criticalHitCombinations.get(fighter).set('lastUsed', 0);

     return;
  }

  criticalHitCombinations.get(fighter).get('keyCodes').add(keyCode);

  return (Date.now() - criticalHitCombinations.get(fighter).get('lastUsed')) > CRITICAL_COMBINATION_USE_TIMEOUT &&
    criticalHitCombinations.get(fighter).get('keyCodes').size === 3;
}

function updateBars(firstFighter, secondFighter) {
  document.getElementById('left-fighter-indicator').style.width = `${healths.get('first') / firstFighter.health * 100}%`;
  document.getElementById('right-fighter-indicator').style.width = `${healths.get('second') / secondFighter.health * 100}%`;
}
