import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (!fighter) {
    return fighterElement;
  }

  const imageElement = createFighterImage(fighter);

  fighterElement.append(...createFighterDescription(fighter), imageElement);

  return fighterElement;
}

function createFighterDescription(fighter) {
  const { name, health, attack, defense } = fighter;

  const nameElement = createElement({ tagName: 'span', className: 'fighter-preview___fighter-name' });
  nameElement.innerText = name;

  const healthElement = createElement({ tagName: 'span', className: 'fighter-preview___fighter-description' });
  healthElement.innerText = `Health: ${health}`;
  const attackElement = createElement({ tagName: 'span', className: 'fighter-preview___fighter-description' });
  attackElement.innerText = `Attack: ${attack}`;

  const defenseElement = createElement({ tagName: 'span', className: 'fighter-preview___fighter-description' });
  defenseElement.innerText = `Defence: ${defense}`;

  return [nameElement, healthElement, attackElement, defenseElement];
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
